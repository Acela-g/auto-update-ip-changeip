#!/bin/bash

#Variables
HOST1=xxxxxxxxx.com
HOST2=www.xxxxxxxx.com
USER=xxxxxxxxxxx
PASSWORD=xxxxxxxxxxx
IPACTUAL=$(host ${HOST1} | cut -d " " -f4)
IPREAL=$(curl ifconfig.me)


if [ "$IPREAL" == "$IPACTUAL" ];
  then
          echo "El IP esta actualizado $IPACTUAL"
  else
      wget -q --http-user=$USER --http-password=$PASSWORD "https://nic.changeip.com/nic/update?cmd=update&hostname=${HOST1}"
      wget -q --http-user=$USER --http-password=$PASSWORD "https://nic.changeip.com/nic/update?cmd=update&hostname=${HOST2}"
      echo "Se actualizo el IP de $HOST1 y $HOST2 a $IPREAL"
fi
